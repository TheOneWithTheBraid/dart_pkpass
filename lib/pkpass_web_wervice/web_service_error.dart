import 'package:http/http.dart';

import 'package:pkpass/pkpass.dart';

abstract class PkPassWebServiceError extends PKPassError {
  PkPassWebServiceError({required super.message});
}

class WebServiceUnavailable extends PkPassWebServiceError {
  WebServiceUnavailable()
      : super(message: 'The PkPass file does not contain any web service.');
}

class WebServiceResponseError extends PkPassWebServiceError {
  final Response response;

  WebServiceResponseError(this.response)
      : super(message: 'Unexpected response from web service');
}

abstract class PKPassError extends Error {
  final String message;

  PKPassError({required this.message});

  @override
  String toString() => 'PKPassError: $message';
}

class InvalidEncodingError extends PKPassError {
  InvalidEncodingError() : super(message: 'Input not in ZIP format.');
}

class ManifestNotFoundError extends PKPassError {
  ManifestNotFoundError()
      : super(message: 'No manifest.json found in PKPass archive.');
}

class ManifestChecksumError extends PKPassError {
  final String expected;
  final String actual;

  ManifestChecksumError({
    required this.expected,
    required this.actual,
  }) : super(
          message:
              'Manifest sha1 checksum missmatch, expected $expected, actual $actual',
        );
}

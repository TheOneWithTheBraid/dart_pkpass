/// Information about a location beacon.
class Beacon {
  /// Unique identifier of a Bluetooth Low Energy location beacon.
  final String proximityUUID;

  /// Major identifier of a Bluetooth Low Energy location beacon.
  final double? major;

  /// Minor identifier of a Bluetooth Low Energy location beacon.
  final double? minor;

  /// Text displayed on the lock screen when the pass is currently relevant.
  /// For example, a description of the nearby location such as
  /// “Store nearby on 1st and Main.”
  final String? relevantText;

  const Beacon({
    required this.proximityUUID,
    this.major,
    this.minor,
    this.relevantText,
  });

  factory Beacon.fromJson(Map<String, Object?> json) => Beacon(
        proximityUUID: json['proximityUUID'] as String,
        major: json['major'] as double?,
        minor: json['minor'] as double?,
        relevantText: json['relevantText'] as String?,
      );
}

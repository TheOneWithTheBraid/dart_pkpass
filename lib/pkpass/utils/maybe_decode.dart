import '../models/pass_structure_dictionary.dart';
import 'color_helper.dart';

abstract class MaybeDecode {
  const MaybeDecode._();

  static int? maybeColor(String? colorCode) {
    if (colorCode == null) return null;
    return fromCssColor(colorCode);
  }

  static DateTime? maybeDateTime(String? timeStamp) {
    if (timeStamp == null) return null;
    return DateTime.tryParse(timeStamp);
  }

  static PassTextAlign? maybeTextAlign(String? align) {
    switch (align) {
      case 'PKTextAlignmentLeft':
        return PassTextAlign.left;
      case 'PKTextAlignmentCenter':
        return PassTextAlign.center;
      case 'PKTextAlignmentRight':
        return PassTextAlign.right;
      default:
        return PassTextAlign.natural;
    }
  }

  static PassTextDateStyle? maybeDateStyle(String? style) {
    switch (style) {
      case 'PKDateStyleNone':
        return PassTextDateStyle.none;
      case 'PKDateStyleShort':
        return PassTextDateStyle.short;
      case 'PKDateStyleMedium':
        return PassTextDateStyle.medium;
      case 'PKDateStyleLong':
        return PassTextDateStyle.long;
      case 'PKDateStyleFull':
        return PassTextDateStyle.full;
      default:
        return null;
    }
  }

  static PassTextNumberStyle? maybeNumberStyle(String? style) {
    switch (style) {
      case 'PKNumberStyleDecimal':
        return PassTextNumberStyle.decimal;
      case 'PKNumberStylePercent':
        return PassTextNumberStyle.percent;
      case 'PKNumberStyleScientific':
        return PassTextNumberStyle.scientific;
      case 'PKNumberStyleSpellOut':
        return PassTextNumberStyle.spellOut;
      default:
        return null;
    }
  }
}

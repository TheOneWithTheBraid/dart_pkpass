abstract class LProjParser {
  const LProjParser._();
  static Map<String, String> parse(String stringsFile) => Map.fromEntries(
        RegExp(r'"((?:\\"|[^"])*)"\s?=\s?"((?:\\"|[^"])*)"\s?;')
            .allMatches(stringsFile)
            .map(
              (match) => MapEntry(match.group(1)!, match.group(2)!),
            ),
      );
}

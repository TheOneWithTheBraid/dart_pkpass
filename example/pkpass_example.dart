import 'dart:io';

import 'package:intl/locale.dart';
import 'package:pkpass/pkpass.dart';

Future<int> main(List<String> args) async {
  print('Using first argument or stdin file name: ');
  final path = args.singleOrNull ?? stdin.readLineSync();
  if (path == null || path.isEmpty) {
    print('Please enter a file name or provide it as single argument.');
    return 1;
  }

  final file = File(path);
  final contents = await file.readAsBytes();

  final pass = await PassFile.parse(contents);

  final logo = pass.getLogo(
    scale: 2,
    locale: Locale.fromSubtags(languageCode: 'fr'),
  );

  print('Logo image blob length: ${logo?.length}');
  print('First barcode: ${pass.metadata.barcodes.firstOrNull?.message}');
  print('Location: ${pass.metadata.locations.firstOrNull?.relevantText}');
  print('Date: ${pass.metadata.relevantDate}');
  print(
    'Boarding pass: ${pass.metadata.boardingPass?.headerFields.firstOrNull?.getLocalizedLabel(pass, Locale.fromSubtags(languageCode: 'tlh'))}',
  );

  return 0;
}
